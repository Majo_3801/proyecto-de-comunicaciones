# Proyecto de comunicaciones


## Implementación de modelo Bluetooth para la transmisión de un mensaje de texto 
En este espacio se almacenarán las simulaciones y código realizado para la realización del Proyecto integrador.

## Integrantes 
 - Chyong Hwa Huang
 - María José Obando
 - Ana Isabel Sabater
## Sistemas de simulación utilizados
Matlab y simulink.


## Estatus del proyecto
Finalizado.
